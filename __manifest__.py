# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.
{
    'name': 'Partners Geolocation with OSM',
    'version': '12.0.0.3',
    'category': 'Sales',
    'author': "Jorels SAS",
    'license': 'LGPL-3',
    'website': 'https://www.jorels.com',
    'description': """
Partners Geolocation with OSM
=============================
    """,
    'depends': ['base',
                'contacts',
                'base_geolocalize',
                'base_geoengine_jorels'
                ],
    'data': [
        'views/res_partner_views.xml',
        'views/geoengine.xml',
    ],
    'installable': True,
}
