# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

# import json
import logging

import requests

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError

# from shapely.wkt import loads

_logger = logging.getLogger(__name__)


def geo_find_here(addr, apikey=False):
    if not addr:
        return None

    if not apikey:
        raise UserError(_('''API key for GeoCoding (Places) required.\n
                          Save this key in System Parameters with key: here.api_key_geocode, value: <your api key>
                          Visit https://www.developer.here.com for more information.
                          '''))

    url = "https://geocode.search.hereapi.com/v1/geocode"
    try:
        result = requests.get(url, params={'apikey': apikey, 'q': addr}).json()
        _logger.debug("Response geocode search api: %s", result)
    except Exception as e:
        raise UserError(_(
            'Cannot contact geolocation servers. Please make sure that your Internet connection is up and running (%s).') % e)

    if 'items' not in result:
        error_msg = _('Unable to geolocate, received an error.\n'
                      '\nHERE made this a paid feature.\n'
                      'You should first enable billing on your HERE account.\n'
                      'Then, go to HERE web, and enable the APIs:\n'
                      'Geocoding Rest.\n')
        raise UserError(error_msg)

    try:
        geo = result['items'][0]['position']
        zip = result['items'][0]['address']['postalCode']
        if 'label' in result['items'][0]['address']:
            street = result['items'][0]['address']['label']
        elif 'street' in result['items'][0]['address']:
            street = result['items'][0]['address']['street']
        else:
            street = ''
        _logger.debug("street: %s", street)

        # return latitude, longitude, postal code, street
        return float(geo['lat']), float(geo['lng']), zip, street
    except (KeyError, ValueError, IndexError):
        return None


def geo_query_address_here(street=None, zip=None, city=None, state=None, country=None):
    if country and ',' in country and (country.endswith(' of') or country.endswith(' of the')):
        # put country qualifier in front, otherwise GMap gives wrong results,
        # e.g. 'Congo, Democratic Republic of the' => 'Democratic Republic of the Congo'
        country = '{1} {0}'.format(*country.split(',', 1))
    return tools.ustr(', '.join(
        field for field in [street, ("%s %s" % (zip or '', city or '')).strip(), state, country]
        if field
    ))


def get_country_two_code(country_three_code):
    try:
        url = "https://restcountries.com/v3.1/alpha/" + country_three_code
        result = requests.get(url).json()
        return result[0]['cca2']
    except Exception as e:
        raise UserError(_('API Error (%s).') % e)


class ResPartner(models.Model):
    _inherit = "res.partner"

    # gps_point = fields.GeoPoint(string="Gps", default=loads("POINT (0.0 0.0)"), readonly=True)
    gps_point = fields.GeoPoint(string="Gps", readonly=False)

    @api.multi
    @api.onchange('gps_point')
    def _onchange_gps_point(self):
        for rec in self:
            gps_point = fields.GeoPoint.get_latlon(rec.env.cr, rec.gps_point)
            _logger.debug("Gps point: %s", str(gps_point))
            if gps_point:
                rec.partner_longitude = gps_point.x
                rec.partner_latitude = gps_point.y
                rec.date_localization = fields.Date.context_today(rec)

    @classmethod
    def _geo_localize_osm(self, cls, apikey, street='', zip='', city='', state='', country=''):
        search = geo_query_address_here(street=street, zip=zip, city=city, state=state, country=country)
        result = geo_find_here(search, apikey)
        if result is None:
            search = geo_query_address_here(city=city, state=state, country=country)
            result = geo_find_here(search, apikey)
        return result

    @api.multi
    def geo_localize_osm(self):
        apikey = self.env['ir.config_parameter'].sudo().get_param('here.api_key_geocode')
        for partner in self.with_context(lang='en_US'):
            # Only the first part of address string
            if partner.street:
                street = partner.street.split(',')[0]
            else:
                street = ''
            result = partner._geo_localize_osm(self, apikey,
                                               street=street,
                                               city=partner.city,
                                               state=partner.state_id.name,
                                               country=partner.country_id.name)

            if result:
                geo_point = fields.GeoPoint.from_latlon(partner.env.cr, result[0], result[1])
                gps_point_debug = fields.GeoPoint.get_latlon(partner.env.cr, geo_point)
                _logger.debug("Gps point: %s", str(gps_point_debug))
                partner.write({
                    'partner_latitude': result[0],
                    'partner_longitude': result[1],
                    'zip': result[2],
                    'street2': result[3],
                    'gps_point': geo_point,
                    'date_localization': fields.Date.context_today(partner)
                })
        return True

    @api.multi
    def geo_address_osm(self):
        apikey = self.env['ir.config_parameter'].sudo().get_param('here.api_key_geocode')
        for rec in self:
            result = rec._geo_address_osm(self, apikey, rec.partner_latitude, rec.partner_longitude)
            if result:
                rec.write({
                    'street2': result[0],
                    'zip': result[1],
                    'city': result[2],
                    'state_id': result[3],
                    'country_id': result[4]
                })
        return True

    @classmethod
    def _geo_address_osm(self, cls, apikey, latitude, longitude):
        for rec in cls:
            if not apikey:
                raise UserError(_('''API key for Reverse geocoding required.\n
                                  Save this key in System Parameters with key: here.api_key_geocode, value: <your api key>
                                  Visit https://www.developer.here.com for more information.
                                  '''))

            url = "https://revgeocode.search.hereapi.com/v1/revgeocode"
            try:
                pos = str(latitude) + ',' + str(longitude)
                result = requests.get(url, params={'apikey': apikey, 'at': pos, 'lang': 'es'}).json()
                _logger.debug("Response reverse geocode search api: %s", result)
            except Exception as e:
                raise UserError(_(
                    'Cannot contact geolocation servers. Please make sure that your Internet connection is up and running (%s).') % e)

            if 'items' not in result:
                error_msg = _('Unable to geolocate, received an error.\n'
                              '\nHERE made this a paid feature.\n'
                              'You should first enable billing on your HERE account.\n'
                              'Then, go to HERE web, and enable the APIs:\n'
                              'Geocoding Rest.\n')
                raise UserError(error_msg)
            _logger.debug("_geo_localize_osm: %s", result)
            try:
                # Street
                if 'label' in result['items'][0]['address']:
                    street = result['items'][0]['address']['label']
                elif 'street' in result['items'][0]['address']:
                    street = result['items'][0]['address']['street']
                else:
                    street = ''

                # Postal code
                zip = result['items'][0]['address']['postalCode']

                # Country
                country_three_code = result['items'][0]['address']['countryCode']
                country_two_code = get_country_two_code(country_three_code)
                country_env = rec.env['res.country']
                country_id = country_env.search([('code', '=', country_two_code)]).id

                # State or department
                state = result['items'][0]['address']['county']
                state_values = state.split(' ')
                state = state_values[len(state_values) - 1]
                state_env = cls.env['res.country.state']
                state_id = state_env.search([('name', '=', state), ('country_id', '=', country_id)]).id

                # City
                city = result['items'][0]['address']['city']
                city_values = city.split(',')
                city = city_values[0]

                # return latitude, longitude, postal code, street
                return street, zip, city, state_id, country_id
            except Exception as e:
                return False
